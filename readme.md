# README

This project contains programming assignments for the  Foundations of Algorithms course.

Assignments are stored in projects within `src/main/java`, where the project is named after the programming assignment (PA) number, e.g. `pa1`, `pa2`, etc. Each problem has its own `.java` file, e.g. `Problem1.java`, `Problem2.java`, etc. Each PA directory may also contain a `readme.md` file containing additional relevant information, such as written explications. 

Example code provided with the CLRS text is included in the `com.mhhe.clrs2e` package.

## Installing & Running

- Prerequisites
    - Jdk 8
    - Gradle (May not be required when using `./gradlew`)
- Install a specific class with `./gradlew build -DmainClass="<projectName>.<className>"`
- Run a specific class with `./gradlew run -DmainClass="<projectName>.<className>"`