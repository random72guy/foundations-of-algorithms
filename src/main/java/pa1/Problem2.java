package pa1;

import java.util.Random;
import java.util.Arrays;

import com.mhhe.clrs2e.BinarySearchTree;
import com.mhhe.clrs2e.InsertionSort;
import java.util.Collections;

public class Problem2 {
    public static void main(String[] args) {
        testTree();
    }

    public static void testTree() {

        // Create an empty tree.
        BinarySearchTreePlus tree = new BinarySearchTreePlus();

        // Populate our tree.
        Integer[] valsToInsert = { 12, 33, 24, 18, 2, 14, 59 };
        System.out.println("Creating tree of " + valsToInsert.length + " elements: " + Arrays.toString(valsToInsert));
        for (Integer val : valsToInsert) 
            tree.insert(val);

        // Visualize our tree.
        System.out.println("Tree: ");
        System.out.println(tree.toString());

        // Print some values to help explain the visualization.
        System.out.println("Tree Root Left: " + tree.root.left.data);
        System.out.println("Tree Root Right: " + tree.root.right.data);

        // Test our methods to see whether they work.
        System.out.println("\nHeight: " + tree.height());
        System.out.println("\nPost-Order Traversal: ");
        tree.runPostOrderTraversal();
        System.out.println("\nNumber of Leaves: " + tree.getNumberOfLeaves());
        System.out.println("\nNumber of Non-Leaves: " + tree.getNumberOfNonLeaves());

    }

    public static class BinarySearchTreePlus extends BinarySearchTree {

        public BinarySearchTreePlus() {
            super();
        }

        public int height() {
            return this.getHeightForSubtree(this.root, 1);
        }

        private int getHeightForSubtree(Node root, int numAncestors) {

            // If we've reached a leaf, return # ancestors
            if (isNil(root.left) && isNil(root.right))
                return numAncestors;

            return Math.max(getHeightForSubtree(root.left, numAncestors + 1),
                    getHeightForSubtree(root.right, numAncestors + 1));

        }

        public void runPostOrderTraversal() {
            this.postOrderTraversal(this.root);
        }

        public void postOrderTraversal(Node root) {
            // NB: This should be identical to this.postorderWalk().

            if (isNil(root))
                return;

            postOrderTraversal(root.left);
            postOrderTraversal(root.right);
            System.out.println(root);

        }

        public int getNumberOfLeaves() {

            return getSubtreeNumLeaves(this.root);

        }

        public int getNumberOfNonLeaves() {
            // This isn't the most efficient implementation, but it's simple to code and
            // leverags existing logic.
            return getSubtreeNodeCount(this.root) - this.getNumberOfLeaves();
        }

        private int getSubtreeNumLeaves(Node root) {

            if (isNil(root))
                return 0;

            // If no children, increment count & return.
            if (isNil(root.left) && isNil(root.right)) {
                // System.out.println("Found leaf:" + root);
                return 1;
            }

            return getSubtreeNumLeaves(root.left) + getSubtreeNumLeaves(root.right);

        }

        private int getSubtreeNodeCount(Node root) {

            if (isNil(root)) {
                return 0;
            }

            return 1 + this.getSubtreeNodeCount(root.left) + this.getSubtreeNodeCount(root.right);

        }

    }

}
