
## Problem 1.a

### O(lg(n)) Search
Binary Search is a lg(n) algorithm (because it runs recursively and halves the size of the values to check at each iteration). For our set, we run Binary Search on two arrays, yeilding O(2lg(n)) = O(lg(n)).

### O(sqrt(n)) Insert
If we have n elements distributed accross two arrays with length l and sqrt(l), n = l + sqrt(l). As n approaches infinity, sqrt(l) becomes sqrt(n). Since we only insert into our smaller array, we need to determine the time complexity of insertion sort on an array of length sqrt(n). 

While insertion sort runs in O(n^2) worst case, our array is presumed to be sorted (except for the single new element being added). Assuming the new element is added to the end of the array, insertion sort will perform one pass through all elements (verifying they are in the right place). Only the last element will be found to be out of place, requiring a comparison with up to n-1 previous elements. Thus, we end up with O(2n) or O(n). We already determined that the length of this smaller array is sqrt(n) (where n is the number of elements in our set), so our insertion operation is O(sqrt(n)).

Additional computation is required to extend the arrays when the smaller array fills. This happens once in every sqrt(n) addition operations, since the smaller array is the square-root the size of the larger. This operation takes O(n^2) worst case, assuming insertion sort is used to merge the smaller and larger arrays. Because of how infrequently this occurs, it becomes negligable as n increases.


## Notes

- For PA1 Problem 2, `BinarySearchTree.java` was slightly modified to expose protected values such as children nodes. 