package pa1;

import java.util.Random;
import java.util.Arrays;
import com.mhhe.clrs2e.InsertionSort;
import java.util.Collections;

public class Problem1 {
    public static void main(String[] args) {
        testSet();
    }

    public static void testSet() {

        Random rng = new Random();
        int rngMax = 100;

        // Create initial array of random values
        Integer[] randomIntsArray = generateRandomArray(5, rngMax);
        System.out.println("Unsorted Input Array: " + Arrays.toString(randomIntsArray));

        // Create set
        Set set = new Set(randomIntsArray);
        System.out.println("Set: \n" + set);

        // Insert elm
        Integer newElm = rng.nextInt(rngMax);
        set.insert(newElm);
        System.out.println("Set post-insert: \n" + set);

        // Search for elm present in shorter array
        set.search(newElm);

        // Search for elm present in longer array
        set.search(set.arrLonger[set.arrLonger.length-1]);

        // Search for non-present elm
        set.search(rngMax+1);


        // Verify arrays increase in size on insert
        int shorterLen = set.arrShorter.length;
        for (int i = 0; i < shorterLen; i++) {
            set.insert(rng.nextInt(rngMax));
        }
        System.out.println("Set after many inserts: \n" + set);

    }

    public static class Set {
        int arrShorterSize = 0; // num elms in shorter array. Needs manual update.
        Integer[] arrShorter; // len = sqrt(arrLonger)
        Integer[] arrLonger = createIntegerArray(4); // Library operations fail on null values. So, init this to 0's.

        InsertionSort insertionSort = new InsertionSort();

        public Set(Integer[] elms) {
            this.arrShorter = elms;
            this.arrShorterSize = elms.length;

            this.arrLonger = createIntegerArray(elms.length * elms.length);
        }

        Integer search(Integer searchFor) {
            // Use Binary Search on both arrays.
            try {
                Integer res = binarySearch(this.arrShorter, 0, this.arrShorter.length, searchFor);
                System.out.println("Found (in shorter array): " + res);
                return res;
            } catch (Exception eShorter) {
                try {
                    Integer res =  binarySearch(this.arrLonger, 0, this.arrLonger.length, searchFor);
                    System.out.println("Found (in longer array): " + res);
                    return res;
                } catch (Exception eLonger) {
                    System.out.println("Not found in array: " + searchFor);
                    return null;
                }
            }
        }

        void insert(int elm) {

            System.out.println("Inserting " + elm);

            // To add an element, you use insertion sort to insert the element into the
            // shorter array. If the short array fills up, re-allocate the shorter and
            // longer arrays and merge the two old arrays into the new longer array. The new
            // short array is empty.

            // First, check if we have room for another elm.
            if (arrShorterSize == arrShorter.length)
                extendArrays();

            addElm(elm);
            sort();
        }

        private void addElm(int elm) {
            arrShorter[arrShorterSize] = elm;
            arrShorterSize++;
        }

        private void sort() {
            insertionSort.sort(arrShorter);
        }

        private void extendArrays() {

            System.out.println("Extending arrays.");

            // Short array is filled.
            // Re-allocate the shorter and longer arrays and
            int shorterLen = arrShorter.length * 2;
            Integer[] newArrShorter = createIntegerArray(shorterLen);
            Integer[] newArrLonger = createIntegerArray(shorterLen * shorterLen);

            // merge the two old arrays into the new longer array.
            mergeArrays(arrShorter, arrLonger, newArrLonger);

            arrShorter = newArrShorter;
            arrLonger = newArrLonger;
        }

        private void mergeArrays(Integer[] shorter, Integer[] longer, Integer[] dest) {

            System.out.println("Merging arrays.");

            // copy shorter over
            for (int i = 0; i < shorter.length; i++)
                dest[i] = shorter[i];

            // copy longer over
            for (int i = 0; i < longer.length; i++) {
                dest[shorter.length + i] = longer[i];
            }
            insertionSort.sort(dest);

            System.out.println("dest post-sort: " + Arrays.toString(dest));

        }

        public String toString() {
            return "Shorter: " + Arrays.toString(this.arrShorter) + "\nLonger: " + Arrays.toString(this.arrLonger);
        }
    }

    private static Integer[] createIntegerArray(int length) {
        return Collections.nCopies(length, 0).toArray(new Integer[0]); // Initialize array with 0's instead of nulls.
    }

    private static Integer[] generateRandomArray(int length, int rngMax) {
        Random rng = new Random();
        Integer[] arr = new Integer[length];
        for (int i = 0; i < length; i++)
            arr[i] = rng.nextInt(rngMax);
        return arr;
    }

    private static Integer binarySearch(Integer[] array, int leftIndex, int rightIndex, Integer searchFor) throws NullPointerException {
        
        // If out of bounds, return null.
        if (rightIndex < 1) return null;

        Integer middleIndex = leftIndex + (rightIndex - 1) / 2;

        if (array[middleIndex] == searchFor) return array[middleIndex];

        if (array[middleIndex] > searchFor) return binarySearch(array, leftIndex, middleIndex - 1, searchFor);
        
        return binarySearch(array, middleIndex + 1, rightIndex, searchFor);
        
    }

}

