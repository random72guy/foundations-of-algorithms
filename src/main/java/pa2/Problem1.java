package pa2;

import java.util.Arrays;
import com.mhhe.clrs2e.InsertionSort;

public class Problem1 {
    public static void main(String[] args) {
        runTest();
    }

    public static void runTest() {

        String[] input = { "COW", "DOG", "SEA", "RUG", "ROW", "MOB", "BOX", "TAB", "BAR", "EAR", "TAR", "DIG", "BIG",
                "TEA", "NOW", "FOX" };

        String[] results = radixSort(input, 3);
        System.out.println("Sorted list: " + Arrays.toString(results));

    }

    private static String[] radixSort(String[] initialInput, Integer numIndices) {

        // For convenience, we choose InsertionSort as our stable sort. A linear-time sort might be used instead, but is not strictly required. (See RadixSort(A, d), text page 198.)
        InsertionSort insertionSort = new InsertionSort();

        // Transform input so we can sort by value at given index. Theta(n)
        RadixSortSet[] input = new RadixSortSet[initialInput.length];
        for (int i = 0; i < initialInput.length; i++) {
            input[i] = new RadixSortSet(initialInput[i]);
        }

        // For each index, run the stable sort.
        for (int i = 0; i < numIndices; i++) {

            // insertionSort
            insertionSort.sort(input);

            System.out.println(String.format("After pass on index %d: %s \n", i, Arrays.toString(input)));


            // Update the index. Adds Theta(n).
            for(RadixSortSet set : input) {
                set.incrementrightOffset();
            }

        }

        // Transform back to String[]. Theta(n)
        String[] returnArray = new String[input.length];
        for (int i = 0; i < input.length; i++) {
            returnArray[i] = input[i].val;
        }

        return returnArray;
    }

    public static class RadixSortSet implements Comparable<RadixSortSet> {

        public Character sortBy;
        public String val;
        private Integer rightOffset = 0;

        public RadixSortSet(String val) {
            this.val = val;
            this.sortBy = this.getCharAtRightOffset();
        }

        public char getCharAtRightOffset() {
            int index = this.val.length() - (1 + this.rightOffset);
            if (index < 0) index = 0;
            return this.val.toCharArray()[index];
        }

        public void incrementrightOffset() {
            this.rightOffset++;
            this.sortBy = getCharAtRightOffset();
        }

        public String toString() {
            return String.format("%s (%s)", this.val, this.sortBy);
        }

        @Override
        public int compareTo(RadixSortSet set) {
            return this.sortBy.compareTo(set.sortBy);
        }
    }

}
