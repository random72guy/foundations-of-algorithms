package pa2;

import java.util.Stack;
import java.util.Arrays;
import com.mhhe.clrs2e.SentinelDLL;

import java.util.Iterator;

public class Problem2 {

    // Create our two doubly-linked lists.

    public static void main(String[] args) {
        runTest();
    }

    public static void runTest() {

        StackSet set = new StackSet();

        // Initialize stack A via PushA(x).
        set.pushA("a");
        set.pushA("b");
        set.pushA("c");
        set.pushA("d");

        // Initialize stack B via PushB(x).
        set.pushB("e");
        set.pushB("f");
        set.pushB("g");
        set.pushB("h");

        System.out.println(String.format("A initialized: \n%s", set.a));
        System.out.println(String.format("B initialized: \n%s", set.b));

        // Test MultiPopA(k) and MultiPopB(k).
        System.out.println("Popping 1 element from A, and 2 from B.");
        Object[] poppedFromA = set.multiPopA(1);
        Object[] poppedFromB = set.multiPopB(2);

        System.out.println(String.format("Popped from A: %s", Arrays.toString(poppedFromA)));
        System.out.println(String.format("Popped from B: %s", Arrays.toString(poppedFromB)));
        System.out.println(String.format("A: \n%s", set.a));
        System.out.println(String.format("B: \n%s\n", set.b));

        // Test Tranfer(k).
        System.out.println("Transfering 2 elements from A to B.");
        Object[] transfered = set.transfer(2);

        System.out.println(String.format("Transfered: %s", Arrays.toString(transfered)));
        System.out.println(String.format("A: \n%s", set.a));
        System.out.println(String.format("B: \n%s", set.b));

        // Verify MultiPopA(k) (and therefore Transfer(k)) works correctly when we run out of item in stack A.
        System.out.println("Transfering 2 more elements from A to B.");
        Object[] transfered2 = set.transfer(2);

        System.out.println(String.format("Transfered: %s", Arrays.toString(transfered2)));
        System.out.println(String.format("A: \n%s", set.a));
        System.out.println(String.format("B: \n%s", set.b));

    }

    static class StackSet {

        // Using Linear Dynamically Linked List from CLRS materials.
        // We could have used generic stacks here, but that seems to defeat the purpose
        // of the assignment, as they already have pop & push functions.
        SentinelDLL a = new SentinelDLL();
        SentinelDLL b = new SentinelDLL();

        public StackSet() {

        }

        private void push(SentinelDLL list, Object x) {
            list.insert(x);
        }

        public void pushA(Object x) {
            this.push(this.a, x);
        }

        public void pushB(Object x) {
            this.push(this.b, x);
        }

        private Object[] multiPop(SentinelDLL list, int k) {

            // Get iterator
            Iterator<Object> iter = list.iterator();

            // Create stack of return values
            Stack<Object> poppedItems = new Stack<Object>();

            for (int i = 0; i < k; i++) {
                Object o = iter.next();
                if (o != null) {
                    iter.remove();
                    poppedItems.push(o);
                }
            }

            return poppedItems.toArray();
        }

        public Object[] multiPopA(int k) {
            return this.multiPop(this.a, k);
        }

        public Object[] multiPopB(int k) {
            return this.multiPop(this.b, k);
        }

        public Object[] transfer(int k) {
            Object[] transferedElms = this.multiPopA(k);
            for (Object elm : transferedElms) {
                this.pushB(elm);
            }
            return transferedElms;
        }

    }

}
